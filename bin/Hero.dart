import 'dart:io';

import 'Selecthero.dart';
void main(List<String> arguments) {
  Hero h = new Hero();
  h.display();
}
class Hero {
 
  var s = new Selecthero();
 SelectAttack(int Select, int Player) {
    switch (Select) {
      case 1:
        if (s.MartialArt[Player] == 'Karate') {
          s.Attack[Player] = s.ATK[Player] + 5;
          s.Rage[Player] = 'Normal Punch 5 dmg';
        } else if (s.MartialArt[Player] == 'MuayThai') {
          s.Attack[Player] = s.ATK[Player] + 8;
          s.Rage[Player] = 'Normal Punch 8 dmg';
        }
        break;
      case 2:
        if (s.MartialArt[Player] == 'Karate') {
          s.Attack[Player] = s.ATK[Player] + 5;
          s.Rage[Player] = 'Normal Kick 5 dmg';
        } else if (s.MartialArt[Player] == 'MuayThai') {
          s.Attack[Player] = s.ATK[Player] + 7;
          s.Rage[Player] = 'Normal Kick 7 dmg';
        }
        break;
      case 3:
        if (s.MartialArt[Player] == 'Karate') {
          s.Attack[Player] = s.ATK[Player] + 10;
          s.Rage[Player] = 'Round Kick 10 dmg';
        } else if (s.MartialArt[Player] == 'MuayThai') {
          s.Attack[Player] = s.ATK[Player] + 10;
          s.Rage[Player] = 'Elbow Slash 10 dmg';
        }
        break;
      case 4:
        if (s.MartialArt[Player] == 'Karate') {
          s.Attack[Player] = s.ATK[Player] + 12;
          s.Rage[Player] = 'Hard Punch 12 dmg';
        } else if (s.MartialArt[Player] == 'MuayThai') {
          s.Attack[Player] = s.ATK[Player] + 15;
          s.Rage[Player] = 'Straight Kick 15 dmg';
        }
        break;
      case 5:
        if (s.MartialArt[Player] == 'Karate') {
          s.Attack[Player] = s.ATK[Player] + 15;
          s.Rage[Player] = 'Front Kick 15 dmg';
        } else if (s.MartialArt[Player] == 'MuayThai') {
          s.Attack[Player] = s.ATK[Player] + 20;
          s.Rage[Player] = 'Uppercut 20 dmg';
        }
        break;
      default:
    }
  }
  die() {
    if (s.HP[0] > s.HP[1]) {
      print('${s.NAME[0]} ชนะการต่อสู้ (คนที่ 1)');
    }
    if (s.HP[1] > s.HP[0]) {
      print('${s.NAME[1]} ชนะการต่อสู้ (คนที่ 2)');
    }
    if (s.HP[0] < s.HP[1]) {
      print('${s.NAME[0]} แพ้การต่อสู้ (คนที่ 1)');
    }
    if (s.HP[1] < s.HP[0]) {
      print('${s.NAME[1]} แพ้การต่อสู้ (คนที่ 2)');
    }
    if (s.HP[0] == s.HP[1]) {
      print('${s.NAME[0]} เสมอการต่อสู้ (คนที่ 1)');
    }
    if (s.HP[1] == s.HP[0]) {
      print('${s.NAME[1]} เสมอการต่อสู้ (คนที่ 2)');
    }
  }
  void display() {
    print('ฮีโร่');
    print('1. Jin กระบวนท่า Karate (HP 150 ATK 25)');
    print('2. Eddy กระบวนท่า Karate (HP 155 ATK 20)');
    print('3. Steve กระบวนท่า MuayThai (HP 130 ATK 30)');
    print('4. Negan กระบวนท่า MuayThai (HP 170 ATK 15)');
    print('เลือกฮีโร่ (คนที่ 1) :');
    int? n1 = int.parse(stdin.readLineSync()!);
    s.SelectHero(n1, 0);
    print('เลือกฮีโร่ (คนที่ 2) :');
    int? n2 = int.parse(stdin.readLineSync()!);
    s.SelectHero(n2, 1);
    print('');
    print('ฮีโร่ Jin/Eddy\t\t\t  ฮีโร่ Steve/Negan');
    print('กระบวนท่า Karate\t\t กระบวนท่า MuayThai');
    print('1. Normal Punch (5 dmg)\t\t1. Normal Punch (8 dmg)');
    print('2. Normal Kick (5 dmg)\t\t2. Normal Kick (7 dmg)');
    print('3. Round Kick (10 dmg)\t\t3. Elbow Slash (10 dmg)');
    print('4. Hard Punch (12 dmg)\t\t4. Straight Kick (15 dmg)');
    print('5. Front Kick (15 dmg)\t\t5. Uppercut (20 dmg)');
    do {
      print('${s.NAME[0]} เลือกกระบวนท่า ${s.MartialArt[0]} (คนที่ 1) :');
      int? n3 = int.parse(stdin.readLineSync()!);
      SelectAttack(n3, 0);
      print('โจมตีด้วยกระบวนท่า ${s.Rage[0]}');
      print('${s.NAME[1]} เลือกกระบวนท่า ${s.MartialArt[1]} (คนที่ 2) :');
      int? n4 = int.parse(stdin.readLineSync()!);
      SelectAttack(n4, 1);
      print('โจมตีด้วยกระบวนท่า ${s.Rage[1]}');
      if (s.HP[0] <= s.Plus[0] && s.Check[0] == 0) {
        s.Attack[0] = s.Attack[0] + 10;
        s.Check[0] = 1;
      }
      if (s.HP[1] <= s.Plus[1] && s.Check[1] == 0) {
        s.Attack[1] = s.Attack[1] + 10;
        s.Check[1] = 1;
      }
      s.HP[0] = s.HP[0] - s.Attack[1];
      s.HP[1] = s.HP[1] - s.Attack[0];
      print('');
      print('ผลการต่อสู้');
      print('${s.NAME[0]} HP เหลือเพียง ${s.HP[0]} (คนที่ 1)');
      print('${s.NAME[1]} HP เหลือเพียง ${s.HP[1]} (คนที่ 2)');
      print('');
    } while (s.HP[0] > 0 && s.HP[1] > 0);
    die();
  }
}

